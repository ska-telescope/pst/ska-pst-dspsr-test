# DSPSR tests

This repo contains pipeline templates intended for running DSPSR functional pipeline tests.

## Usage

### Pipeline variables

The dspsr-test CI pipeline loads [dspsr.env](./dspsr.env) variables to a [dotenv](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) artefact to allow dynamic configuration during a pipeline trigger.
Consider [Gitlab's variable precedence](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence) when updating the [.gitlab-ci.yaml](./.gitlab-ci.yml). Configurable variables are as follows:

| Variable | Default | Description |
| --- | --- | --- |
| DSPSR_REPO | git://git.code.sf.net/p/dspsr/code | The DSPSR repository. Used for grabbing the commit hash used as the tag for the DSPSR_IMAGE |
| DSPSR_BRANCH | master | The DSPSR repository branch. Used for grabbing the commit hash used as the tag for the DSPSR_IMAGE |
| DSPSR_IMAGE | registry.gitlab.com/ska-telescope/pst/ska-pst-dspsr/dspsr | The OCI image used for running DSPSR functional tests |
| DSPSR_TAG | "" | Populated during a pipeline trigger.   |

### Gitlab downstream Pipelines

For external GitLab repositories which desires to trigger the pipelines configured in ska-pst-dspsr-test, the below sample config can be used by adding adding it to the .gitlab-ci.yml

```yaml
# This example overrides the ska-pst-dspsr-test CI environment variable DSPSR_BRANCH.
sample_downstream_override_dspsr_test_branch:
  trigger:
    project: ska-telescope/pst/ska-pst-dspsr-test
  variables:
    # The DSPSR_BRANCH value of master is replaced by the source commit branch.
    DSPSR_BRANCH: $CI_COMMIT_BRANCH

# This example overrides the ska-pst-dspsr-test OCI configuration.
sample_downstream_override_dspsr_test_oci_configuration:
  trigger:
    project: ska-telescope/pst/ska-pst-dspsr-test
  variables:
    # The DSPSR_IMAGE value of registry.gitlab.com/ska-telescope/pst/ska-pst-dspsr/ska-pst-dspsr is replaced with artefact.skao.int/ska-pst-dspsr.
    DSPSR_IMAGE: artefact.skao.int/ska-pst-dspsr
    # The DSPSR_TAG value of the dspsr latest commit hash is replaced with the value provided.
    DSPSR_TAG: 0.0.1
```

### Gitlab http API Calls

This can be used by remote repositories if limitations are experienced when using GitLab's downstream pipeline triggers.

```bash
# Set Gitlab Access Token
READ PRIVATE_TOKEN

# Set ska-pst-dspsr-test branch
#     NOTE: configure when developing this repository, otherwise set it as main.
READ DSPSR_TEST_BRANCH

# Perform API CALL
curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
"https://gitlab.com/api/v4/projects/15202140/pipeline?ref=$DSPSR_TEST_BRANCH"

# Sample output as follows
# "https://gitlab.com/api/v4/projects/15202140/pipeline?ref=at3-670-custom-triggers"
# {"id":1311138488,"iid":60,"project_id":15202140,"sha":"ac119c7f2074cb12ed2c930926c952c941019798","ref":"at3-670-custom-triggers","status":"created","source":"api","created_at":"2024-05-30T01:26:19.361Z","updated_at":"2024-05-30T01:26:19.361Z","web_url":"https://gitlab.com/ska-telescope/pst/ska-pst-dspsr-test/-/pipelines/1311138488","before_sha":"0000000000000000000000000000000000000000","tag":false,"yaml_errors":null,"user":{"id":2711420,"username":"jesmigel","name":"Jesmigel Cantos","state":"active","locked":false,"avatar_url":"https://secure.gravatar.com/avatar/b15995458574de28cbe00ec3119b1600f330f7e7e3c9361182f87358c3ffbee8?s=80\u0026d=identicon","web_url":"https://gitlab.com/jesmigel"},"started_at":null,"finished_at":null,"committed_at":null,"duration":null,"queued_duration":null,"coverage":null,"detailed_status":{"icon":"status_created","text":"Created","label":"created","group":"created","tooltip":"created","has_details":true,"details_path":"/ska-telescope/pst/ska-pst-dspsr-test/-/pipelines/1311138488","illustration":null,"favicon":"/assets/ci_favicons/favicon_status_created-4b975aa976d24e5a3ea7cd9a5713e6ce2cd9afd08b910415e96675de35f64955.png"}}
```

## TODO

The DSPSR functional pipeline tests are yet to be implemented. The following is a checklist for its implementation.

- [ ] Confirm storage for involved datasets
- [ ] Confirm platform: Gitlab Container vs K8s Pod. Platform depends on storage solution.
- [ ] Confirm trigger rules CI_PIPELINE_SOURCE. see https://docs.gitlab.com/ee/ci/yaml/
